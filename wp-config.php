<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'captus' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define( 'AUTH_KEY',         '>YM3}=kimJY{S)+ER|Nw9wRN10!li6#1KU(:Rt9NZ3LJ^-C3MR8F:6o4?imqVkEW' );
define( 'SECURE_AUTH_KEY',  '6:)V%,kNfE<-v^&fz)P^amY06q716t???$+sSl)}?PqjBDFKKA{|Ls5|nl?}H3u,' );
define( 'LOGGED_IN_KEY',    ' ^3ykTwZmapXt{1$n]Q!0hJ;:6axl{?;FaMJ%uC;.;.^LE#?2&fai|!MGU@3{b +' );
define( 'NONCE_KEY',        'mM#M=XJ%+Un/0u,$<qtrbqF?B7[=h9P_C_Fo.h&!K$c& mM(4vh!;eQl&g=zHXa|' );
define( 'AUTH_SALT',        'BLs,Qp9!/uG:k*lWAEOf_Ja@6bkd]8J{wj7Wq%#I0F-G20N,gX5W7_r?vh6<PGNS' );
define( 'SECURE_AUTH_SALT', 'p `kW7(aC6Sv#x_7FK3{l:gCd8**0!QF&/W`pXOJ)>$UH`A]iM}Eg(GoeCdV;?7n' );
define( 'LOGGED_IN_SALT',   '(7.hW})(w#/@am}3qjLaDEm),k*m.NV9{t8l/U;Yp2`rx$6^)!Dn}{yN)vEFM-OM' );
define( 'NONCE_SALT',       '<=#!*[G6^q}5eiUf|&.pN&,^}pAFZhk;3|^|iJU]xn*feO$c:+3PT8(BI2U|e7MG' );

/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
